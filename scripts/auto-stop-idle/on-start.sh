#!/bin/bash
echo "Starting ..."
set -e
# OVERVIEW
# This script stops a SageMaker notebook once it's idle for more than 1 hour (default time) # You can change the idle time for stop using the environment variable below.
# If you want the notebook the stop only if no browsers are open, remove the --ignore-connections flag # # Note that this script will fail if either condition is not met #   1. Ensure the Notebook Instance has internet connectivity to fetch the example config #   2. Ensure the Notebook Instance execution role permissions to SageMaker:StopNotebookInstance to stop the notebook #       and SageMaker:DescribeNotebookInstance to describe the notebook.
# # PARAMETERS
IDLE_TIME=3600
echo "Fetching the autostop script"
wget https://bitbucket.org/egoenergy/amazon-sagemaker-notebook-instance-lifecycle-config-samples/raw/d47a3c87d951985d9799d6bb8ee704041552fd5b/scripts/auto-stop-idle/autostop.py echo "Starting the SageMaker autostop script in cron"
(crontab -l 2>/dev/null; echo "*/5 * * * * /usr/bin/python $PWD/autostop.py --time $IDLE_TIME --ignore-connections") | crontab echo "Changing cloudwatch configuration"
curl https://bitbucket.org/egoenergy/amazon-sagemaker-notebook-instance-lifecycle-config-samples/raw/d47a3c87d951985d9799d6bb8ee704041552fd5b/scripts/auto-stop-idle/on-start.sh | sudo bash -s auto-stop-idle /home/ec2-user/SageMaker/auto-stop-idle.log